import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class foodGUI {
    private JPanel root;
    private JButton paellaButton;
    private JButton pizzaButton;
    private JButton pastaButton;
    private JButton risottoButton;
    private JButton lasagnaButton;
    private JButton saladButton;
    private JRadioButton dollarRadioButton;
    private JRadioButton yenRadioButton;
    private JButton checkoutButton;
    private JTextPane orderedItemsList;
    private JLabel totalPayText;
    private double totalPayNum;
    private String currency = "$";
    private double exchangeRate = 110;
    private ArrayList<JButton> buttonList = new ArrayList<>() {
        {
            add(paellaButton);
            add(pizzaButton);
            add(pastaButton);
            add(risottoButton);
            add(lasagnaButton);
            add(saladButton);
        }
    };
    private ArrayList<String> foodName = new ArrayList<>() {
        {
            add("Paella");
            add("Pizza");
            add("Pasta");
            add("Risotto");
            add("Lasagna");
            add("Salad");
        }
    };
    private ArrayList<Double> foodPrice = new ArrayList<>() {
        {
            add(7.0);
            add(6.0);
            add(5.0);
            add(4.5);
            add(4.0);
            add(3.5);
        }
    };
    private ArrayList<JRadioButton> currencyList = new ArrayList<>() {
        {
            add(dollarRadioButton);
            add(yenRadioButton);
        }
    };
    private ArrayList<String> currencySign = new ArrayList<>() {
        {
            add("$");
            add("￥");
        }
    };
    private ArrayList<Integer> orderItems = new ArrayList<>();

    public foodGUI() {
        totalPayText.setText("0");
        setButtonText();
        for (int i = 0; i < buttonList.size(); i++) {
            int idx = i;
            buttonList.get(i).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orderConfirmation(foodName.get(idx), foodPrice.get(idx));
                }
            });
        }
        paellaButton.setIcon(new ImageIcon(this.getClass().getResource("img/paella.jpg")));
        pizzaButton.setIcon(new ImageIcon(this.getClass().getResource("img/pizza.jpg")));
        pastaButton.setIcon(new ImageIcon(this.getClass().getResource("img/pasta.jpg")));
        risottoButton.setIcon(new ImageIcon(this.getClass().getResource("img/risotto.jpg")));
        lasagnaButton.setIcon(new ImageIcon(this.getClass().getResource("img/lasagna.jpg")));
        saladButton.setIcon(new ImageIcon(this.getClass().getResource("img/salad.jpg")));
        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkoutConfirmation();
            }
        });
        for (int i = 0; i < currencyList.size(); i++) {
            int idx = i;
            currencyList.get(i).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(currencyList.get(idx).isSelected()) {
                        currency = currencySign.get(idx);
                        setButtonText();
                        updateOrderedItemsList();
                        updateTotalPay();
                    }
                }
            });
        }
    }

    public void setButtonText() {
        for (int i = 0; i < buttonList.size(); i++) {
            if (currency == "$") {
                buttonList.get(i).setText("<html><center>" + foodName.get(i) + "<br>" + currency + foodPrice.get(i) + "</center></html>");
            }
            else if (currency == "￥") {
                buttonList.get(i).setText("<html><center>" + foodName.get(i) + "<br>" + currency + (int)(foodPrice.get(i) * exchangeRate) + "</center></html>");
            }
            buttonList.get(i).setFont(new Font("Aerial", Font.PLAIN, 20));
        }
    }

    public void orderConfirmation(String food, double price) {
        int orderConfirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + food + " ?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if (orderConfirmation == 0) {
            addOrderItems(food, price);
            updateOrderedItemsList();
            addPriceToTotal(price);
            updateTotalPay();
            printOrderReceived(food);
        }
    }

    public void addOrderItems(String food, double price) {
        orderItems.add(foodName.indexOf(food));
    };

    public void updateOrderedItemsList() {
        String currentText = "";
        for (int i = 0; i < orderItems.size(); i++) {
            int idx = orderItems.get(i);
            if (currency == "$") {
                currentText += foodName.get(idx) + " " + currency + foodPrice.get(idx) + "\n";
            }
            else if (currency == "￥") {
                currentText += foodName.get(idx) + " " + currency + (int)(foodPrice.get(idx) * exchangeRate) + "\n";
            }
        }
        orderedItemsList.setText(currentText);
    }

    public void addPriceToTotal(double price) {
        totalPayNum += price;
    }

    public void updateTotalPay() {
        if (currency == "$") {
            totalPayText.setText(currency + totalPayNum);
        }
        else if (currency == "￥") {
            totalPayText.setText(currency + (int)(totalPayNum * exchangeRate));
        }
    }

    static void printOrderReceived(String food) {
        JOptionPane.showMessageDialog(null, "Order for " + food + " received!");
    }

    public void checkoutConfirmation() {
        int checkoutConfirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?", "Checkout Confirmation", JOptionPane.YES_NO_OPTION);
        if (checkoutConfirmation == 0) {
            printThanksMessage();
            clearListAndPay();
        }
    }

    public void printThanksMessage() {
        if (currency == "$") {
            JOptionPane.showMessageDialog(null, "Thank you! The total price is " + currency + totalPayNum);
        }
        else if (currency == "￥") {
            JOptionPane.showMessageDialog(null, "Thank you! The total price is " + currency + (int)(totalPayNum * exchangeRate));
        }
    }

    public void clearListAndPay() {
        orderItems.clear();
        orderedItemsList.setText("");
        totalPayNum = 0;
        totalPayText.setText("0");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodGUI");
        frame.setContentPane(new foodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}